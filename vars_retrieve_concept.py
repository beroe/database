#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Will find either a concept or a list of samples from a dive.

To find a concept run with a COMMA-separated list of concept names (no space).

To find samples from a dive, run with the appropriate flag as true,
and send in a space- or comma-delimited list of dive numbers.
NOTE: Comma-separated lists cannot have spaces too...

You can search in three ways:
 -d : get dive summary
 -s : get samples collected during dive
 -c : get all annotations of concepts for all dives
 -a : get all associations between two or more groups, separated by +
       secret option: put 1 at the end for full table (nothing is summary)
 -k : get all species for a higher taxon (takes only one name)
       secret option: put 1 at the end for quoted csv, 2 for full table
 
  
Usage: 
	{0} -s D422 R500
	{0} -d D422,v360 
	{0} -c Aulacoctena,"Bathyctena chuni"
	{0} -d v{{2770..2880}}    # get a dive summary for all dives between those two numbers
	{0} -k Narcomed, Scyphozo # All species for both Narcos and Scyphos 
	{0} -a Narcome, Scyphoz + Amphipod # Associations between Narcos and Amphipods
	
version 1.47: Changed order of fields in output, added field number for cutting
version 1.46: Cleaned up output and improved library installation instructions
version 1.45: Added associations and reformatted output
version 1.4: Added extraction of species from higher taxon
version 1.3: Handle sample numbers in V3875-D5 format
version 1.2: Flag to perform different queries at run time
version 1.1: Parses the V2132 format. Can also take dive number w/o vehicle
  [haddock at MBARI dot org]
Requires: pymssql module, freetds-dev, and unixODBC 
Installation instructions are a comment in the source code below...

"""
# main() code is at the bottom
# INSTALLATION: pre-requisites for OSX, in this order
#     Command Line Tools from https://developer.apple.com/downloads/ 
#     Install HomeBrew with this exact command: 
#         ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"
#     Install pip with this exact command:
#         sudo easy_install pip
#     brew install unixodbc
#     brew install --with-unixodbc freetds
#     sudo pip install pymssql

import time
import sys
import pymssql



def parsedivenumbers(Dive,shortname=False):
	Dive = Dive.strip()
	DiveDict = dict(zip(list("TVRD"),["Tiburon","Ventana","Doc Ricketts","Doc Ricketts"]))
	if shortname:
		DiveDict = dict(zip(list("TVRD"),["tibr","vnta","docr","docr"]))
	if "-" in Dive:
		DFields = Dive.split("-")
		Dive = DFields[0]
		Sample = DFields[1]
	else:
		Sample = ""
	if Dive[0].isdigit():
		Veh= "%"
		Num = Dive.strip()
	else:
		Veh = DiveDict[Dive[0].upper()]
		Num = Dive[1:].strip()
	return Veh,Num,Sample

def getsampleinfo(SampleID):
	"""Incomplete?"""
	DQuery = "SELECT DISTINCT SampleRefName, DescriptionComment from MBARI_Samples.dbo.Sample where SampleID < 1000; "

	execquery(DQuery, mydatabase="EXPD")

def getconceptfromKB(higher_taxa,style=1):
	""" Retrieve all subordinate taxonomic concepts from the knowledgebase
 Style 1 (default) is quoted and comma delimited. 
 Style 0 is a list with one concept per line
 Style 2 is the full table with concept numbers and higher-order classes
 """
	ConString = """ parent_name LIKE '%{}%' """
	Splits = higher_taxa.split(",")
	
	if len(Splits) > 1:
		ConList = Splits
	else:
		ConList = [higher_taxa]
	SearchNames = ""
	outstr=""
	NumCons = 0
	delimit=["\n",", ",""][style] # zero gets newline
	for Con in ConList:
		Constraint = ConString.format(Con.strip())
		# print "CONSTRAINT: ", Constraint
	
# Using knowledgebase (VARS_KB), select all the species for a group
		Cquery = """
		WITH org_name AS (
	        SELECT DISTINCT
	            parent.id AS parent_id, parentname.ConceptName as parent_name,
	            child.id AS child_id, childname.ConceptName as child_name
	        FROM
	            Concept parent RIGHT OUTER JOIN 
	            Concept child ON child.ParentConceptID_FK = parent.id LEFT OUTER JOIN
	            ConceptName childname ON childname.ConceptID_FK = child.id LEFT OUTER JOIN
	            ConceptName parentname ON parentname.ConceptID_FK = parent.id
	        WHERE
	            childname.NameType = 'Primary' AND
	            parentname.NameType = 'Primary' ), 
	jn AS (   SELECT            parent_id, parent_name, child_id, child_name FROM org_name 
			WHERE ({}) 
			UNION ALL SELECT C.parent_id, C.parent_name, C.child_id, C.child_name FROM jn AS p 
			JOIN org_name AS C ON C.parent_id = p.child_id ) 
	SELECT DISTINCT jn.parent_id, jn.parent_name, jn.child_id, jn.child_name 
	FROM jn ORDER BY 1;
	""".format(Constraint)

		NumRecs, SpeciesList = execquery(Cquery, mydatabase="VARS_KB")
		SearchNames += " and ".join(Con)
		NumCons += NumRecs
		sys.stderr.write("## Found %d concepts for query of %s\n"%(NumRecs,Con))
		if style == 2:
			outstr = SpeciesList
			break
		try:
			# Get the full name of the query to include it too, e.g. Chrysaora and not just speciesprint
			if style == 1:
				FirstName = "'{}'".format(SpeciesList.split("\t")[1])
				SpNames = [ "'{}'".format(F.split("\t")[3]) for F in SpeciesList.rstrip().split("\n")]
			else:
				FirstName = SpeciesList.split("\t")[1]
				SpNames = [ F.split("\t")[3] for F in SpeciesList.rstrip().split("\n")]
		except IndexError:
			# sys.stderr.write("No lower concepts found for {}\n".format(higher_taxa))
			if style == 1:
				FirstName = "'{}'".format(Con)
			else:
				FirstName = Con
			SpNames = []
			
		if outstr:
			head=delimit
		else:
			head=""
		outstr += head + delimit.join([ FirstName ] + SpNames)

	return NumCons,outstr
	
def findassociation(conceptstrings):
	try:
		HostString,AssocString = " ".join(conceptstrings).split("+")
		HostList = HostString.split()
		AssocList = AssocString.split()
	except ValueError:
		sys.exit("** To search for associations, provide two or more concepts separated by a plus\n")
	
	""" Make your own custom query """
	# print "HostString",  (getconceptfromKB([HostString]))
	# print "AssocString", AssocList
	Outstr=""
	TotalNum=0
	
	
	# This returns a tuple with number and string...
	HostCons = getconceptfromKB(HostString)[1]
	AssocCons = getconceptfromKB(AssocString)[1]
	sys.stderr.write("Finding associations for {0}... \n                    WITH {1}...\n".format(HostCons[:100],AssocCons[:100]))
	
	# Need to search both ways: fish annotated with jelly and jelly annotated with fish...
	for Flipper in ([HostCons,AssocCons],[AssocCons,HostCons]):
		FromCon = """ ann.ConceptName IN ({}) """.format(Flipper[0])
		# ToCon = """ ann.ToConcept IN ({},'Marine organism') """.format(Flipper[1])
		ToCon = """ ( (ann.ToConcept IN ({0}) ) OR (REPLACE(ann.Associations,'-',' ' ) IN ({0}) ) ) """.format(Flipper[1])
		# FromCon = """ ann.ConceptName IN ({}) """.format("'Chrysaora'")
		# ToCon = """ ann.ToConcept IN ({}) """.format("'Doryteuthis opalescens'")
		# Take all args as a string, split on commas (genus species have spaces)
	
		Aquery = """ SELECT
		ann.RovName, ann.DiveNumber,ann.DEPTH, ann.ConceptName,ann.Latitude, ann.Longitude, ann.Temperature, ann.Oxygen, ann.Salinity,
		ann.RecordedDate, ann.Image, ann.TapeTimeCode, ann.Zoom,
		ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
		ann.VideoArchiveSetID_FK AS vasid, ann.ShipName,
		ann.Associations,ann.videoArchiveName, ann.CameraDirection,
		ann.ChiefScientist, ann.FieldWidth, ann.Light, ann.Notes,
		ann.Observer, ann.ToConcept
		FROM Annotations AS ann
		WHERE (
		( ({0}) AND ({1}) ) 
		AND ( ann.linkName LIKE '%association%')   
		) """.format(FromCon,ToCon)
		# print "query: ",Aquery
		# AND (( ann.linkName LIKE '%association%') OR (ann.linkName LIKE '%commensal%'))  

		NumFound,ResultStr = execquery(Aquery)

		TotalNum += NumFound 
		Outstr += ResultStr

	Fields = """RovName\tDiveNumber\tDEPTH\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\t\
	RecordedDate\tImage\tEpochSecs\tTapeTimeCode\tZoom\tObservationID_FK\tLinkValue\tlinkName\t\
	VideoArchiveSetID_FK\tShipName\tObservationID_FK\tAssociations\tvideoArchiveName\tCameraDirection\t\
	ChiefScientist\tFieldWidth\tNotes\tObserver\tAssocConcept\n"""
	L = Fields.split("\t")
	while '' in L:
			L.remove('')
	NumFields = "\t".join(["".join([str(i+1),j]) for i,j in enumerate(L)])
	
	# head = """RovName	DiveNumber	Depth	ConceptName	Latitude	Longitude	Temperature	Oxygen	EpochSecs	Salinity	RecordedDate	Image	TapeTimeCode	Zoom	ObservationID_FK	LinkValue	linkName	VideoArchiveSetID_FK	ShipName	Associations	videoArchiveName	CameraDirection	ChiefScientist	FieldWidth	Light	Notes	Observer	AssocConcept"""
	# print Fields
	# sys.stderr.write("## Found %d associations for %s...\n" % (TotalNum,conceptstrings))

	return TotalNum, NumFields + Outstr
	
def getsamples(DiveList):
	
	# removed Image, but add back in if you want Image URL
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	#DiveList = DiveListAsString.split(",")
	SQuery = """
	SELECT DISTINCT
	      CONVERT(varchar(22), RecordedDate, 120) as DateTime24,
	      RovName, DiveNumber, ConceptName, Associations,
	      Depth, Latitude, Longitude, TapeTimeCode, ISNULL(Observer, '') AS Observer,
	      AnnotationMode, ObservationID_FK, LinkName, LinkValue
	FROM  dbo.Annotations
	WHERE (RovName like '{0}') AND (DiveNumber = {1})
	AND (LinkName LIKE '%sample-reference%') {2}
	ORDER BY TapeTimeCode, DateTime24 ; """
	#sys.stderr.write("Finding samples...\n")
	SHead = "DateTime24	RovName	DiveNumber	ConceptName	Associations	Depth	Latitude	Longitude	TapeTimeCode	Observer	AnnotationMode	ObservationID_FK	SampleRefName"
	if __name__== "__main__":
		print SHead
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if "," in DiveList[0]:
		DiveList = DiveList[0].split(",") 
	for Dive in DiveList:
		ROVName,DiveNum,Sample = parsedivenumbers(Dive)
		if Sample:
			SampleString = "AND (LinkValue LIKE '%{}') ".format(Sample.lower())
		else:
			SampleString = ""
		NumFound, Outstr = execquery(SQuery.format(ROVName, DiveNum, SampleString), mydatabase="VARS")
		TotalFound += NumFound
		AllOut += Outstr
	return AllOut
	sys.stderr.write("## Found %d samples for the query...\n" % (TotalFound))

	
def getdivesummary(DiveList):
	# removed Image, but add back in if you want Image URL
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	#DiveList = DiveListAsString.split(",")
	SQuery = """
	SELECT DISTINCT
		  chiefscientist, shipname, rovname,  divenumber, avgrovlat, avgrovlon, divestartdtg, maxpressure
	FROM  dbo.DiveSummary
	WHERE (RovName like '{0}') AND (DiveNumber = {1})
	"""

	SHead = "ChiefScientist	ShipName	RovName	DiveNumber	AvgROVLatitude	AvgROVLongitude	DiveStartTime	MaxPressure"
	print SHead
	if "," in DiveList[0]:
		DiveList = DiveList[0].split(",") 
	NumFound = 0
	TotalFound = 0
	for Dive in DiveList:
		ROVName,DiveNum,S = parsedivenumbers(Dive, shortname = True)
		# print SQuery.format(ROVName,DiveNum)
		NumFound,Outstr = execquery(query = SQuery.format(ROVName, DiveNum), mydatabase="EXPD")
		TotalFound +=NumFound
		print Outstr.rstrip("\n")
	sys.stderr.write("## Found %s dive summaries...\n" % (TotalFound))

def findconcept(conceptstrings):
	ConString = """ ann.ConceptName like '%%%s%%' """
	# Take all args as a string, split on commas (genus species have spaces)
	Splits = " ".join(conceptstrings).split(",")
	ConList = [ConString % (Con.strip()) for Con in Splits]
	Constraint = "( %s )" % (" OR ".join(ConList))
	# Concept = " ".join(sys.argv[1:])
	query = """ SELECT
	ann.RovName, ann.DiveNumber,ann.DEPTH, ann.ConceptName,ann.Latitude, ann.Longitude, ann.Temperature, ann.Oxygen, ann.Salinity,
	ann.RecordedDate, ann.Image, ann.TapeTimeCode, ann.Zoom,
	ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
	ann.VideoArchiveSetID_FK AS vasid, ann.ShipName,
	ann.Associations,ann.videoArchiveName, ann.CameraDirection,
	ann.ChiefScientist, ann.FieldWidth, ann.Light, ann.Notes,
	ann.Observer,ann.ToConcept
	FROM
	Annotations AS ann
	where
	%s """ % Constraint

	Fields = """RovName\tDiveNumber\tDEPTH\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\t\
	RecordedDate\tImage\tEpochSecs\tTapeTimeCode\tZoom\tObservationID_FK\tLinkValue\tlinkName\t\
	VideoArchiveSetID_FK\tShipName\tObservationID_FK\tAssociations\tvideoArchiveName\tCameraDirection\t\
	ChiefScientist\tFieldWidth\tNotes\tObserver\tToConcept\n"""

	L = Fields.split("\t")
	while '' in L:
			L.remove('')
	NumFields = "\t".join(["".join([str(i+1),j]) for i,j in enumerate(L)])
	sys.stderr.write("Finding all annotations for %s...\n" % conceptstrings)
	# head = """RovName	DiveNumber	Depth	ConceptName	Latitude	Longitude	Temperature	Oxygen	EpochSecs	Salinity	RecordedDate	Image	TapeTimeCode	Zoom	ObservationID_FK	LinkValue	linkName	VideoArchiveSetID_FK	ShipName	Associations	videoArchiveName	CameraDirection	ChiefScientist	FieldWidth	Light	Notes	Observer	ToConcept"""
	print NumFields
	NumFound,Outstr = execquery(query)
	print Outstr
	sys.stderr.write("## Found %d annotations for %s...\n" % (NumFound,conceptstrings))

def execquery(query, mydatabase="VARS"):
	serverlookup = {
		"EXPD"	 : "solstice.shore.mbari.org",
		"VARS"	 : "equinox.shore.mbari.org",
		"VARS_KB": "equinox.shore.mbari.org"
	}

	# grab the right server name, depending on what database is being used...
	servername = serverlookup[mydatabase]
	username = "GETUSERNAMEFROMBRIAN"
	pw = "GETPASSWORDFROMBRIAN"


	# Try the ** operator with the parameters as a dictionary
	"""config = {
  'user': 'scott',
  'password': 'tiger',
  'host': '127.0.0.1',
  'database': 'employees',
  'raise_on_warnings': True,
}
cnx = mysql.connector.connect(**config)
cnx.close()
"""

	conn = pymssql.connect(host=servername, user=username, password=pw, database=mydatabase, as_dict=False)
	cur = conn.cursor()

	#Write all records to one big file:
	# to write to a different file per dive, uncomment the "write to individual files lines below..."

	NumRecords=0

	cur.execute(query)
	outstr=""
	for row in cur:
		NumRecords += 1
		#### THIS HAS TO BE FIXED??
		# date_time = row[8]
		# try:
		# 	epoch= time.mktime(date_time.timetuple())
		# except:
		# 	epoch = 999
		# #epoch = int(time.mktime(time.strptime(date_time, timepattern)))
		strlist = ["%s"%(x) for x in row]
		# strlist.insert(8,str(epoch) )
		#print "%s\t%s\t%s" % (row[7],row[8],row[9])
		#print "\t".join(strlist)
		outstr += "\t".join(strlist)
		outstr += "\n"
		#print epoch
		#print "\t".join(row)
		#print "ID=%d, Name=%s" % (row['id'], row['name'])

	conn.close()
	return NumRecords,outstr

def summarizeassoc(instring):
	""" Split the associations to a subset of pairs"""

	allstr = instring.split("\n")[1:]
	OutSet = {}
	for Line in allstr:
		Values = Line.split("\t")
		if len(Values)>25:
			OutKey = Values[2] + " + " + Values[26]
			OutSet[OutKey] = OutSet.get(OutKey,0) + 1
	OK = sorted(OutSet.keys())
	for k in OK:
		print "%8d  %s" % (OutSet[k],k)
	
### START OF PROGRAM
def main():
	
	if len(sys.argv)<=1:
		sys.stderr.write(__doc__.format(sys.argv[0].split("/")[-1]))
	else:
		if sys.argv[1] == "-c":
			findconcept(sys.argv[2:])
			if len(sys.argv) > 3:
				sys.stderr.write("** Concepts should be comma-separated or surrounded in quotes.\n Space-separated lists will not work as expected")
		elif sys.argv[1] == "-s":
			
			print( getsamples(sys.argv[2:]).rstrip("\n") )
		elif sys.argv[1] == "-d":
			getdivesummary(sys.argv[2:])
		elif sys.argv[1] == "-k":
			if sys.argv[-1] in ("1","2"):
				print( getconceptfromKB(" ".join(sys.argv[2:-1]), style=int(sys.argv[-1]))[1])
			else:
				print( getconceptfromKB(" ".join(sys.argv[2:]), style=0)[1])
		elif sys.argv[1] == "-a":
			style = 0
			if sys.argv[-1] in ("1","2"):
				assocnum, assocstr = findassociation(sys.argv[2:-1]) 
				style = 1
			else:
				assocnum, assocstr = findassociation(sys.argv[2:]) 

			sys.stderr.write("\n## Found %d annotations for %s (and vice versa)...\n" % (assocnum," ".join(sys.argv[2:]) ))
			if style:
				print assocstr
			else:
				summarizeassoc(assocstr)
			
		else:
			sys.stderr.write(__doc__.format(sys.argv[0]))

if __name__ == "__main__":
	main()

"""
DiveSummary in expd has the following fields:
COLUMN_NAME
avgrovlat
avgrovlon
avgshiplat
avgshiplon
chiefscientist
ctdconfigid
ctdlastupdate
ctdlastwho
ctdlastwhy
ctdo2optodecount
ctdo2sbecount
ctdpcount
ctdplotsent
ctdscount
ctdtcount
ctdxmisscount
diveenddtg
diveid
divenumber
divestartdtg
expdid
id
maxpressure
maxrovlat
maxrovlon
maxshiplat
maxshiplon
minrovlat
minrovlon
minshiplat
minshiplon
rovid
rovname
shipid
shipname
"""

